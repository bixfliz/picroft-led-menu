# -*- coding:utf-8 -*-
import sys
import SH1106
import time
import config
import traceback
import os
import RPi.GPIO as GPIO

import time
from datetime import datetime 
import subprocess
import pytz 

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
#sys.path.insert(1, '/home/pi/mycroft-core/') 
from mycroft.util import play_mp3

CENTRAL_TIME = pytz.timezone('America/Chicago') 

CISTERCIAN_RIGHT = 55
CISTERCIAN_TOP = 20
CISTERCIAN_SCALE = 1

big_number_mode = 0

def draw_time_in_cistercian_numerals():
  datetime_central = datetime.now(CENTRAL_TIME) 
  current_time = datetime_central.strftime("%I%M")
  ones=current_time[3:4]
  tens=current_time[2:3]
  hundreds=current_time[1:2]
  thousands=current_time[0:1]
  
  #clear area
  draw.rectangle((CISTERCIAN_RIGHT-11*CISTERCIAN_SCALE, CISTERCIAN_TOP-3*CISTERCIAN_SCALE, CISTERCIAN_RIGHT+11*CISTERCIAN_SCALE,CISTERCIAN_TOP+23*CISTERCIAN_SCALE), outline=255, fill=1)

  #outline
  if big_number_mode==0:
    draw.line([(CISTERCIAN_RIGHT-11*CISTERCIAN_SCALE,CISTERCIAN_TOP-3*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT+11*CISTERCIAN_SCALE,CISTERCIAN_TOP-3*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT+11*CISTERCIAN_SCALE,CISTERCIAN_TOP-3*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT+11*CISTERCIAN_SCALE,CISTERCIAN_TOP+23*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT-11*CISTERCIAN_SCALE,CISTERCIAN_TOP-3*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT-11*CISTERCIAN_SCALE,CISTERCIAN_TOP+23*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT-11*CISTERCIAN_SCALE,CISTERCIAN_TOP+23*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT+11*CISTERCIAN_SCALE,CISTERCIAN_TOP+23*CISTERCIAN_SCALE)], fill = 0)
  
  #center line
  draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  if ones=='1':
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif ones=='2':
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE)], fill = 0)
  elif ones=='3':
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE)], fill = 0)
  elif ones=='4':
    #4
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif ones=='5':
    #5
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif ones=='6':
    #6
    draw.line([(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif ones=='7':
    #7
    # straight lines
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP)], fill = 0)
    # curvy lines
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP+3*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT,CISTERCIAN_TOP), (CISTERCIAN_RIGHT+8*CISTERCIAN_SCALE,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)] , start = 270, end = 360, fill =0, width=1) 
  elif ones=='8':
    #8
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 6)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP)], fill = 0)

    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP+3*CISTERCIAN_SCALE)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT,CISTERCIAN_TOP), (CISTERCIAN_RIGHT+8*CISTERCIAN_SCALE,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)] , start = 0, end = 90, fill =0, width=1) 
  elif ones=='9':
    #9
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 6)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-4*CISTERCIAN_SCALE,CISTERCIAN_TOP), (CISTERCIAN_RIGHT+4*CISTERCIAN_SCALE,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)] , start = 270, end = 450, fill =0, width=1) 

  if tens=='1':
    #10
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif tens=='2':
    #20
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE)], fill = 0)
  elif tens=='3':
    #30
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE)], fill = 0)
  elif tens=='4':
    #40
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif tens=='5':
    #50
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif tens=='6':
    #60
    draw.line([(CISTERCIAN_RIGHT - 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 4*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
  elif tens=='7':
    #70
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP)], fill = 0)

    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT-4*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT-8*CISTERCIAN_SCALE,CISTERCIAN_TOP+3*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT-8*CISTERCIAN_SCALE,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-8*CISTERCIAN_SCALE,CISTERCIAN_TOP), (CISTERCIAN_RIGHT,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)] , start = 180, end = 270, fill =0, width=1) 
  elif tens=='8':
    #80
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 6)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP)], fill = 0)
    
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 6*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 3*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-8*CISTERCIAN_SCALE,CISTERCIAN_TOP), (CISTERCIAN_RIGHT,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)] , start = 90, end = 180, fill =0, width=1) 
  elif tens=='9':
    #90
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 6)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 6),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-4*CISTERCIAN_SCALE,CISTERCIAN_TOP), (CISTERCIAN_RIGHT+4*CISTERCIAN_SCALE,CISTERCIAN_TOP+6*CISTERCIAN_SCALE)] , start = 90, end = 270, fill =0, width=1) 

  if hundreds=='1':
    #100
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif hundreds=='2':
    #200
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE)], fill = 0)
  elif hundreds=='3':
    #300
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE)], fill = 0)
  elif hundreds=='4':
    #400
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif hundreds=='5':
    #500
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif hundreds=='6':
    #600
    draw.line([(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif hundreds=='7':
    #700
    # straight lines
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 20)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 20)], fill = 0)
    # curve lines
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP+20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP+14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP+17*CISTERCIAN_SCALE)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT,CISTERCIAN_TOP+14*CISTERCIAN_SCALE), (CISTERCIAN_RIGHT+8*CISTERCIAN_SCALE,CISTERCIAN_TOP+20*CISTERCIAN_SCALE)] , start = 0, end = 90, fill =0, width=1) 
  elif hundreds=='8':
    #800
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 14)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 20)], fill = 0)

    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 17*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT + 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT,CISTERCIAN_TOP+14*CISTERCIAN_SCALE), (CISTERCIAN_RIGHT+8*CISTERCIAN_SCALE,CISTERCIAN_TOP+20*CISTERCIAN_SCALE)] , start = 270, end = 360, fill =0, width=1) 
  elif hundreds=='9':
    #900
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 14)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 20)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20),(CISTERCIAN_RIGHT + 8,CISTERCIAN_TOP + 20)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-4*CISTERCIAN_SCALE,CISTERCIAN_TOP+14*CISTERCIAN_SCALE), (CISTERCIAN_RIGHT+4*CISTERCIAN_SCALE,CISTERCIAN_TOP+20*CISTERCIAN_SCALE)] , start = 270, end = 450, fill =0, width=1) 

  if thousands=='1':
    #1000
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif thousands=='2':
    #2000
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE)], fill = 0)
  elif thousands=='3':
    #3000
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE)], fill = 0)
  elif thousands=='4':
    #4000
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif thousands=='5':
    #5000
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif thousands=='6':
    #6000
    draw.line([(CISTERCIAN_RIGHT - 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
  elif thousands=='7':
    #7000
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 20)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 20)], fill = 0)

    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP+20*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP+14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP+17*CISTERCIAN_SCALE)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-8*CISTERCIAN_SCALE,CISTERCIAN_TOP+14*CISTERCIAN_SCALE), (CISTERCIAN_RIGHT,CISTERCIAN_TOP+20*CISTERCIAN_SCALE)] , start = 90, end = 180, fill =0, width=1) 
  elif thousands=='8':
    #8000
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 14)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 20)], fill = 0)

    draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 4*CISTERCIAN_SCALE,CISTERCIAN_TOP + 14*CISTERCIAN_SCALE)], fill = 0)
    draw.line([(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 17*CISTERCIAN_SCALE),(CISTERCIAN_RIGHT - 8*CISTERCIAN_SCALE,CISTERCIAN_TOP + 20*CISTERCIAN_SCALE)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-8*CISTERCIAN_SCALE,CISTERCIAN_TOP+14*CISTERCIAN_SCALE), (CISTERCIAN_RIGHT,CISTERCIAN_TOP+20*CISTERCIAN_SCALE)] , start = 180, end = 270, fill =0, width=1) 
  elif thousands=='9':
    #9000
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 14)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 14),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 20)], fill = 0)
    #draw.line([(CISTERCIAN_RIGHT,CISTERCIAN_TOP + 20),(CISTERCIAN_RIGHT - 8,CISTERCIAN_TOP + 20)], fill = 0)
    draw.arc([(CISTERCIAN_RIGHT-4*CISTERCIAN_SCALE,CISTERCIAN_TOP+14*CISTERCIAN_SCALE), (CISTERCIAN_RIGHT+4*CISTERCIAN_SCALE,CISTERCIAN_TOP+20*CISTERCIAN_SCALE)] , start = 90, end = 270, fill =0, width=1) 


#GPIO define
RST_PIN        = 25
CS_PIN         = 8
DC_PIN         = 24

KEY_UP_PIN     = 6 
KEY_DOWN_PIN   = 19
KEY_LEFT_PIN   = 5
KEY_RIGHT_PIN  = 26
KEY_PRESS_PIN  = 13

KEY1_PIN       = 21
KEY2_PIN       = 20
KEY3_PIN       = 16

SnowStorm = 0

# 240x240 display with hardware SPI:
disp = SH1106.SH1106()
disp.Init()

# Clear display.
disp.clear()
# time.sleep(1)

#init GPIO
# for P4:
# sudo vi /boot/config.txt
# gpio=6,19,5,26,13,21,20,16=pu
GPIO.setmode(GPIO.BCM) 
GPIO.setup(KEY_UP_PIN,      GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Input with pull-up
GPIO.setup(KEY_DOWN_PIN,    GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_LEFT_PIN,    GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_RIGHT_PIN,   GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_PRESS_PIN,   GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY1_PIN,        GPIO.IN, pull_up_down=GPIO.PUD_UP)      # Input with pull-up
GPIO.setup(KEY2_PIN,        GPIO.IN, pull_up_down=GPIO.PUD_UP)      # Input with pull-up
GPIO.setup(KEY3_PIN,        GPIO.IN, pull_up_down=GPIO.PUD_UP)      # Input with pull-up

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
image = Image.new('1', (disp.width, disp.height), "WHITE")

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)
font10 = ImageFont.truetype('Font.ttf',13)

last_weather_update = time.time()
last_cistercian_time_update = time.time()
draw_time_in_cistercian_numerals()

try:
  weather = os.popen('curl -s wttr.in/~North+Little+Rock?format="%t++%C"').read()
except:
  print("except")

button_1_up = 0

draw.text((0,0), 'Start Sleep Sound >', font = font10, fill = 0)
draw.text((0,47), weather, font = font10, fill = 0)

# try:
while 1:
  if not GPIO.input(KEY1_PIN): # button pressed
    if SnowStorm == 0:
      if big_number_mode == 0:
        draw.rectangle((0, 0, 127,63), outline=255, fill=1)
        draw.text((0,0), 'Stop Sleep Sound >', font = font10, fill = 0)
      play_mp3("/opt/mycroft/skills/play-snow-storm-sound.bixfliz/wind.mp3")
      SnowStorm = 1
    else:
      if big_number_mode == 0:
        draw.rectangle((0, 0, 127,63), outline=255, fill=1)
        draw.text((0,0), 'Start Sleep Sound >', font = font10, fill = 0)
      os.system("killall mpg123")
      SnowStorm = 0
    time.sleep(2)
  if not GPIO.input(KEY2_PIN): # button pressed
    if big_number_mode == 0:
      # big clock on
      big_number_mode = 1
      CISTERCIAN_RIGHT = 55
      CISTERCIAN_TOP = 5
      CISTERCIAN_SCALE = 2
      time.sleep(1)
    else:
      # big clock off
      big_number_mode = 0
      CISTERCIAN_RIGHT = 55
      CISTERCIAN_TOP = 20
      CISTERCIAN_SCALE = 1
    # clear all
    draw.rectangle((1, 1, 128,64), outline=255, fill=1)
    time.sleep(1)

  # update time every 10 seconds
  if ((time.time() - last_cistercian_time_update) > 10):
    last_cistercian_time_update = time.time()
    draw_time_in_cistercian_numerals()
    draw.rectangle((0, 47, 128,64), outline=255, fill=1)
    draw.text((0,47), weather, font = font10, fill = 0)
  
  disp.ShowImage(disp.getbuffer(image))
  if ((time.time() - last_weather_update) > 120):
    last_weather_update = time.time()
    try:
      weather = os.popen('curl -s wttr.in/~North+Little+Rock?format="%t++%C"').read()
    except:
      print("except")


  time.sleep(0.1)
    
# except:
	# print("except")
# GPIO.cleanup()
